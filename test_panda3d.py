# This examples shows how to load several robots in panda3d_viewer.
# Note: this feature requires panda3d_viewer to be installed, this can be done using
# pip install --user panda3d_viewer

import pinocchio as pin
import sys
import numpy as np

from os.path import dirname, join, abspath

# add path to the example-robot-data package
from example_robot_data import loadTalos, loadRomeo, loadICub, loadTiago
from example_robot_data import loadSolo, loadHyQ, loadHector, loadPanda,loadKinova

# import visualizer
from panda3d_viewer import Viewer
from pinocchio.visualize.panda3d_visualizer import Panda3dVisualizer


# open a GUI window
viewer = Viewer(window_title='python-pinocchio')

robot = loadKinova()

viz = Panda3dVisualizer(robot.model, robot.collision_model, robot.visual_model)
viz.initViewer(viewer=viewer)  # attach to a viewer's scene
viz.loadViewerModel(group_name=robot.model.name)

q0 = robot.q0#np.array([0.00138894,5.98736e-05,-0.00259058,   -1.69992, -6.64181e-05,    1.56995,-5.1812e-05, 0, 0])
viz.display(q0)


# calculate the jacobian
data = robot.model.createData()
pin.framesForwardKinematics(robot.model,data,q0)
pin.computeJointJacobians(robot.model,data, q0)
J = pin.getFrameJacobian(robot.model, data, robot.model.getFrameId("panda_link8"), pin.LOCAL_WORLD_ALIGNED)
print(np.round(J,3))

viewer.join()