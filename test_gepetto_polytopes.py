# NOTE: this example needs gepetto-gui to be installed
# usage: launch gepetto-gui and then run this test

from cProfile import label
import pinocchio as pin
import numpy as np
import sys
from pinocchio.visualize import GepettoVisualizer
from example_robot_data import loadSolo, loadHyQ, loadHector, loadPanda,loadKinova,loadUR

robot = loadKinova()
viz = GepettoVisualizer(robot.model, robot.collision_model, robot.visual_model)

# Initialize the viewer.
try:
    viz.initViewer()
except ImportError as err:
    print("Error while initializing the viewer. It seems you should install gepetto-viewer")
    print(err)
    sys.exit(0)

try:
    viz.loadViewerModel("pinocchio")
except AttributeError as err:
    print("Error while loading the viewer model. It seems you should start gepetto-viewer")
    print(err)
    sys.exit(0)


# Display a robot configuration.
# q0 = pin.neutral(robot.model)
q0 = robot.q0
viz.display(q0)

# calculate the jacobian
data = robot.model.createData()
pin.framesForwardKinematics(robot.model,data,q0)
pin.computeJointJacobians(robot.model,data, q0)
J = pin.getFrameJacobian(robot.model, data, robot.model.getFrameId(robot.model.frames[-1].name), pin.LOCAL_WORLD_ALIGNED)
print(np.round(J,3))
J = J[:3,:]


# polytope python module
import pycapacity.robot as pycap
# get max torque
t_max = robot.model.effortLimit
t_min = -t_max
# calculate force polytope
vertices, faces_index =  pycap.force_polytope_withfaces(J, t_max, t_min)
faces = pycap.face_index_to_vertex(vertices,faces_index)
# calculate force ellipsoid
radii, rot_mat =  pycap.force_ellipsoid(J, t_max)


# plotting the polytope
import matplotlib.pyplot as plt
import pycapacity.visual as pcvis # pycapacity visualisation tools

fig = plt.figure()
# draw faces and vertices
ax = pcvis.plot_polytope_vertex(plt=plt, vertex=vertices, label='force polytope',color='blue')
pcvis.plot_polytope_faces(ax=ax, faces=faces, face_color='blue', edge_color='blue', alpha=0.2)
# draw the ellipsoid
pcvis.plot_ellipsoid(radii=radii, rotation=rot_mat,ax=ax,color='yellow', edge_color='yellow', alpha=0.2, label="force ellopsoid")

plt.tight_layout()
plt.legend()
plt.show()

